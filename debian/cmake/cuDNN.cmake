add_library(cuDNN::cuDNN SHARED IMPORTED)
set_target_properties(
	cuDNN::cuDNN PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "/usr/include"
	IMPORTED_LOCATION "/usr/lib/@DEB_HOST_MULTIARCH@/libcudnn.so"
)
