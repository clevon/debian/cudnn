add_library(cuDNN::adv_train SHARED IMPORTED)
set_target_properties(
	cuDNN::adv_train PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "/usr/include"
	IMPORTED_LOCATION "/usr/lib/@DEB_HOST_MULTIARCH@/libcudnn_adv_train.so"
)
