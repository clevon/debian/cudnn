add_library(cuDNN::cnn_infer SHARED IMPORTED)
set_target_properties(
	cuDNN::cnn_infer PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "/usr/include"
	IMPORTED_LOCATION "/usr/lib/@DEB_HOST_MULTIARCH@/libcudnn_cnn_infer.so"
)
