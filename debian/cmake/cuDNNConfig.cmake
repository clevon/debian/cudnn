include(CMakeFindDependencyMacro)
set(known_components "adv_infer" "adv_train" "cnn_infer" "cnn_train" "cuDNN" "ops_infer" "ops_train")

foreach(component ${cuDNN_FIND_COMPONENTS})
	if(TARGET cuDNN::${component})
		continue()
	endif()

	if(NOT "${component}" IN_LIST known_components)
		message(AUTHOR_WARNING "Requested unknown cuDNN component ${component}.\nKnown components: ${known_components}")
		if(cuDNN_FIND_REQUIRED_${component})
			set(cuDNN_FOUND FALSE)
		endif()
		continue()
	endif()

	include("${CMAKE_CURRENT_LIST_DIR}/${component}.cmake" OPTIONAL RESULT_VARIABLE found)

	if(NOT found AND cuDNN_FIND_REQUIRED_${component})
		set(cuDNN_FOUND ${found})
	endif()
endforeach()
